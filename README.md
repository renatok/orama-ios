# Desafio Órama
> Aplicativo iOS para pessoas que querem investir em fundos de investimentos.

[![Swift Version][swift-image]][swift-url]
[![CocoaPods Compatible](https://img.shields.io/cocoapods/v/EZSwiftExtensions.svg)](https://img.shields.io/cocoapods/v/LFAlertController.svg)  
[![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

Projeto criado em linguagem Swift para o Desafio Órama, utilizando arquitetura MVC com suas pastas devidamente organizadas. 
Foram utilizados `Object Mappers` para as Models, algumas classes de extensão para um melhor desacoplamento, arquivo dedicado para as variáveis constantes do projeto, para funções úteis e também para gerenciameno de Storage. 

Utilização de View Code para construção da UI.


## Features

- [x] Lista de Fundos de investimentos, consumido através do endpoint disponiblizado pela Órama;
- [x] Detalhe do invesstimento, com possibilidade de compra de cotas;
- [x] Histórico de compra de cotas para investimento.

## Requirements

- iOS 10.0+
- Xcode 12.0.1

## Installation

#### CocoaPods
Necessário [CocoaPods](http://cocoapods.org/) para instalação/atualização das seguintes bibliotecas, que estão descritas também em `Podfile`:

```ruby
  pod 'Alamofire', '~> 4.7'
  pod 'AlamofireObjectMapper', '~> 5.2.1'
  pod 'ImageLoader'
  pod 'UserDefaults++', '~> 1.0'
```

## Meta

Renato Kuroe – renatolangley@gmail.com


[https://bitbucket.org/renatok/](https://bitbucket.org/renatok/)

[swift-image]:https://img.shields.io/badge/swift-5.0-orange.svg
[swift-url]: https://swift.org/
[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[codebeat-image]: https://codebeat.co/badges/c19b47ea-2f9d-45df-8458-b2d952fe9dad
[codebeat-url]: https://codebeat.co/projects/github-com-vsouza-awesomeios-com
