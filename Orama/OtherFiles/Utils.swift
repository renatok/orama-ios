//
//  Utils.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import UIKit

class Utils: NSObject {
    
    // Value string to currency format
    public static func formatCurrency(value: String) -> String {
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.init(identifier: "pt-BR")
        formatter.numberStyle = .currency
        
        let convertedNumber = NSNumber.init( value: Double(value) ?? 0)

        if let formattedTipAmount = formatter.string(from: convertedNumber as NSNumber) {
            return formattedTipAmount
        }
        
        return "R$ 0,00"
    }

    // Currency format to float value
    public static func currencyToFloat(currency: String) -> Float {
//        var result = currency.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        var valueFormatted = currency.filter("0123456789".contains)
        valueFormatted.insert(".", at: valueFormatted.index(valueFormatted.endIndex, offsetBy: -2))
        let newValue = (valueFormatted as NSString).floatValue
        return newValue
    }
    
    // Date string to Brazilian date format
    public static func formatDate(dateString: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date: Date? = dateFormatterGet.date(from: dateString)
         
        return dateFormatter.string(from: date!)
    }
    
    // Date type to Brazilian date format
    public static func dateToString(date: Date) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
    
    // Simple alert passing custom message
    public static func showAlert(vc: UIViewController, message: String) {
        let alert = UIAlertController(title: "Aviso", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            alert.dismiss(animated: true)
        }))
        vc.present(alert, animated: true)
    }
    
}
