//
//  Constants.swift
//  Orama
//
//  Created by Renato Kuroe on 25/09/20.
//

struct Constants {
    
    // Endpoints
    static let URL_FUND_DETAIL_FULL = "https://s3.amazonaws.com/orama-media/json/fund_detail_full.json"

    // Main View Controller
    static let APP_TITLE = "Órama"
    static let INITIAL_DATE = "Data inicial: "
    static let MINIMUM_APPLICATION = "Aplicação mínima: "
    static let HISTORIC = "Histórico"
    
    // Fund Detail View Controller
    static let DETAIL_TITLE = "Detalhes"
    static let BUY = "INVESTIR"
    static let HOW_MUCH = "Quanto deseja investir?"
    static let CANCEL = "Cancelar"
    static let BUY_SUCCESS = "Parabéns! Investimento realizado com sucesso!"
    static let MINIMUM_VALUE = "O valor mínimo de aplicação é de "
    
    // Historic View Controller
    static let VIEW_TITLE = "Histórico"
    static let APPLICATION_MADE = "Valor da aplicação realizada: "
    static let CLOSE = "Fechar"
    static let APPLICATION_DATE = "Data da aplicação: "
    static let NO_HISTORIC = "Sem histórico de compras."
    
    // Alert error messages
    static let ALERT_ERROR_TIMEOUT = "Tempo de limite esgotado."
    static let ALERT_ERROR_CONNECTION_FAILED = "Falha na conexão com a internet."
    static let ALERT_ERROR_GENERIC = "Erro ao receber dados."

}
