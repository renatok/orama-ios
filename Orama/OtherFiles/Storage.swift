//
//  HistoricSave.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import Foundation
import UserDefaults_

struct Storage {
    static let fundBoughtList = UserDefault<[FundBought]>(key: "fundBoughtList")
}

struct FundBought: PlistStorable {
    let name: String
    let added: Date
    var amount: String
}


class HistoricStorage: NSObject {
    public static func addToFundBoughtList(name: String, added: Date, amount: String) {
        var boughtList = Storage.fundBoughtList.get() ?? []
        boughtList.append(FundBought(name: name, added: added, amount: amount))
        Storage.fundBoughtList.persist(boughtList)
    }
}



