//
//  FundManager.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class FundManager: Mappable {
    
    var description: String?
    var id: Int?
    var full_name: String?
    var logo: String?
    var slug: Bool?
    var name: Int?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        description      <- map["description"]
        id               <- map["id"]
        full_name        <- map["full_name"]
        logo             <- map["logo"]
        slug             <- map["slug"]
        name             <- map["name"]
    }
}
