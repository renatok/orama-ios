//
//  Documents.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Documents: Mappable {
    
    var position: Int?
    var document_type: String?
    var name: String?
    var document_url: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        position               <- map["position"]
        document_type          <- map["document_type"]
        name                   <- map["name"]
        document_url           <- map["document_url"]
    }
}
