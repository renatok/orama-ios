//
//  Operability.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Operability: Mappable {
    
    var has_operations_on_thursdays: Bool?
    var retrieval_liquidation_days_type: String?
    var application_quotation_days_type: String?
    var retrieval_quotation_days: Int?
    var anticipated_retrieval_quotation_days_type: String?
    var anticipated_retrieval_quotation_days_str: String?
    var retrieval_quotation_days_type: String?
    var anticipated_retrieval_quotation_days: Int?
    var has_operations_on_wednesdays: Bool?
    var minimum_balance_permanence: String?
    var has_operations_on_mondays: Bool?
    var has_grace_period: Bool?
    var application_time_limit: String?
    var retrieval_time_limit: String?
    var has_operations_on_fridays: Bool?
    var anticipate_retrieval_liquidation_days: Int?
    var minimum_initial_application_amount: String?
    var anticipate_retrieval_liquidation_days_type: String?
    var retrieval_special_type: String?
    var anticipate_retrieval_liquidation_days_str: String?
    var application_quotation_days_str: String?
    var grace_period_in_days_str: String?
    var retrieval_liquidation_days_str: String?
    var minimum_subsequent_retrieval_amount: String?
    var retrieval_direct: Bool?
    var retrieval_liquidation_days: Int?
    var application_quotation_days: Int?
    var max_balance_permanence: String?
    var maximum_initial_application_amount: String?
    var has_operations_on_tuesdays: Bool?
    var minimum_subsequent_application_amount: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        has_operations_on_thursdays                      <- map["has_operations_on_thursdays"]
        retrieval_liquidation_days_type                  <- map["retrieval_liquidation_days_type"]
        application_quotation_days_type                  <- map["application_quotation_days_type"]
        retrieval_quotation_days                         <- map["retrieval_quotation_days"]
        anticipated_retrieval_quotation_days_type        <- map["anticipated_retrieval_quotation_days_type"]
        anticipated_retrieval_quotation_days_str         <- map["anticipated_retrieval_quotation_days_str"]
        retrieval_quotation_days_type                    <- map["retrieval_quotation_days_type"]
        anticipated_retrieval_quotation_days             <- map["anticipated_retrieval_quotation_days"]
        has_operations_on_wednesdays                     <- map["has_operations_on_wednesdays"]
        minimum_balance_permanence                       <- map["minimum_balance_permanence"]
        has_operations_on_mondays                        <- map["has_operations_on_mondays"]
        has_grace_period                                 <- map["has_grace_period"]
        application_time_limit                           <- map["application_time_limit"]
        retrieval_time_limit                             <- map["retrieval_time_limit"]
        has_operations_on_fridays                        <- map["has_operations_on_fridays"]
        anticipate_retrieval_liquidation_days            <- map["anticipate_retrieval_liquidation_days"]
        minimum_initial_application_amount               <- map["minimum_initial_application_amount"]
        anticipate_retrieval_liquidation_days_type       <- map["anticipate_retrieval_liquidation_days_type"]
        retrieval_special_type                           <- map["retrieval_special_type"]
        anticipate_retrieval_liquidation_days_str        <- map["anticipate_retrieval_liquidation_days_str"]
        application_quotation_days_str                   <- map["application_quotation_days_str"]
        grace_period_in_days_str                         <- map["grace_period_in_days_str"]
        retrieval_liquidation_days_str                   <- map["retrieval_liquidation_days_str"]
        minimum_subsequent_retrieval_amount              <- map["minimum_subsequent_retrieval_amount"]
        retrieval_direct                                 <- map["retrieval_direct"]
        retrieval_liquidation_days                       <- map["retrieval_liquidation_days"]
        application_quotation_days                       <- map["application_quotation_days"]
        max_balance_permanence                           <- map["max_balance_permanence"]
        maximum_initial_application_amount               <- map["maximum_initial_application_amount"]
        has_operations_on_tuesdays                       <- map["has_operations_on_tuesdays"]
        minimum_subsequent_application_amount            <- map["minimum_subsequent_application_amount"]

    }
}

