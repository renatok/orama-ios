//
//  Specification.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Specification: Mappable {
    
    var fund_suitability_profile: FundProfile?
    var fund_risk_profile: FundProfile?
    var is_qualified: Bool?
    var fund_type: String?
    var fund_class: String?
    var fund_macro_strategy: FundProfile?
    var fund_class_anbima: String?
    var fund_main_strategy: FundProfile?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        fund_suitability_profile      <- map["fund_suitability_profile"]
        fund_risk_profile             <- map["fund_risk_profile"]
        is_qualified                  <- map["is_qualified"]
        fund_type                     <- map["fund_type"]
        fund_class                    <- map["fund_class"]
        fund_macro_strategy           <- map["fund_macro_strategy"]
        fund_class_anbima             <- map["fund_class_anbima"]
        fund_main_strategy            <- map["fund_main_strategy"]
    }
    
    class FundProfile: Mappable {
        
        var score_range_order: Int?
        var name: String?

        required init?(map: Map) {

        }
        
        func mapping(map: Map) {
            score_range_order          <- map["score_range_order"]
            name                       <- map["name"]
        }
    }
    
    class FundStrategy: Mappable {
        
        var explanation: String?
        var id: Int?
        var name: String?

        required init?(map: Map) {

        }
        
        func mapping(map: Map) {
            explanation               <- map["explanation"]
            id                        <- map["id"]
            name                      <- map["name"]
        }
    }
}
