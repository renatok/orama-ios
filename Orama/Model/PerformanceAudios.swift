//
//  PerformanceAudios.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class PerformanceAudios: Mappable {
    
    var reference_date: String?
    var soundcloud_id: Int?
    var permalink_url: String?
    var id: Int?
    var title: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        reference_date    <- map["reference_date"]
        soundcloud_id     <- map["soundcloud_id"]
        permalink_url     <- map["permalink_url"]
        id                <- map["id"]
        title             <- map["title"]
    }
}
