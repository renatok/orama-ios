//
//  Fee.swift
//  Orama
//
//  Created by Renato Kuroe on 25/09/20.
//

import Foundation
import ObjectMapper

class Fees: Mappable {
    
    var maximum_administration_fee: String?
    var anticipated_retrieval_fee_value: String?
    var administration_fee: String?
    var anticipated_retrieval_fee: String?
    var performance_fee: String?
    var has_anticipated_retrieval: Bool?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        maximum_administration_fee               <- map["maximum_administration_fee"]
        anticipated_retrieval_fee_value          <- map["anticipated_retrieval_fee_value"]
        administration_fee                       <- map["administration_fee"]
        anticipated_retrieval_fee                <- map["anticipated_retrieval_fee"]
        performance_fee                          <- map["performance_fee"]
        has_anticipated_retrieval                <- map["has_anticipated_retrieval"]
    }
}
