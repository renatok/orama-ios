//
//  Benchmark.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Benchmark: Mappable {
    
    var id: Int?
    var name: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        id     <- map["id"]
        name   <- map["name"]
    }
}
