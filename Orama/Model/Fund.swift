//
//  Fund.swift
//  Orama
//
//  Created by Renato Kuroe on 25/09/20.
//

import Foundation
import ObjectMapper

class Fund: Mappable {
    
    var initial_date: String?
    var performance_audios: PerformanceAudios?
    var fees: Fees?
    var is_simple: Bool?
    var description_seo: String?
    var operability: Operability?
    var full_name: String?
    var opening_date: String?
    var id: String?
    var is_closed: Bool?
    var simple_name: String?
    var documents: [Documents]?
    var specification: Specification?
    var quota_date: String?
    var tax_classification: String?
    var cnpj: String?
    var description: Description?
    var performance_videos: [Video]?
    var is_active: String?
    var benchmark: Benchmark?
    var orama_standard: Bool?
    var slug: String?
    var fund_situation: FundSituation?
    var volatility_12m: String?
    var strategy_video: Video?
    var profitabilities: Profitabilities?
    var closed_to_capture_explanation: String?
    var net_patrimony_12m: String?
    var is_closed_to_capture: Bool?
    var fund_manager: FundManager?
    
    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        initial_date                    <- map["initial_date"]
        performance_audios              <- map["performance_audios"]
        fees                            <- map["fees"]
        is_simple                       <- map["is_simple"]
        description_seo                 <- map["description_seo"]
        operability                     <- map["operability"]
        full_name                       <- map["full_name"]
        opening_date                    <- map["opening_date"]
        id                              <- map["id"]
        is_closed                       <- map["is_closed"]
        simple_name                     <- map["simple_name"]
        documents                       <- map["documents"]
        specification                   <- map["specification"]
        quota_date                      <- map["quota_date"]
        tax_classification              <- map["tax_classification"]
        cnpj                            <- map["cnpj"]
        description                     <- map["description"]
        performance_videos              <- map["performance_videos"]
        is_active                       <- map["is_active"]
        benchmark                       <- map["benchmark"]
        orama_standard                  <- map["orama_standard"]
        slug                            <- map["slug"]
        fund_situation                  <- map["fund_situation"]
        volatility_12m                  <- map["volatility_12m"]
        strategy_video                  <- map["strategy_video"]
        profitabilities                 <- map["profitabilities"]
        closed_to_capture_explanation   <- map["closed_to_capture_explanation"]
        net_patrimony_12m               <- map["net_patrimony_12m"]
        is_closed_to_capture            <- map["is_closed_to_capture"]
        fund_manager                    <- map["fund_manager"]
    }
}
