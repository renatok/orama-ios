//
//  FundSituation.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class FundSituation: Mappable {
    
    var code: Int?
    var name: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        code   <- map["code"]
        name   <- map["name"]
    }
}

