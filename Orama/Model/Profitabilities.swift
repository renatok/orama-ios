//
//  Profitabilities.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Profitabilities: Mappable {
    
    var m60: String?
    var m48: String?
    var m24: String?
    var m36: String?
    var month: String?
    var m12: String?
    var year: String?
    var day: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        m60     <- map["m60"]
        m48     <- map["m48"]
        m24     <- map["m24"]
        m36     <- map["m36"]
        month   <- map["month"]
        m12     <- map["m12"]
        year    <- map["year"]
        day     <- map["day"]
    }
}
