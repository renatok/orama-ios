//
//  PerformanceVideos.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Video: Mappable {
    
    var description: String?
    var title: String?
    var url: String?
    var published: String?
    var id: Int?
    var enabled_for_tvorama: Bool?
    var youtube_id: Int?
    var thumbnail: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        description           <- map["description"]
        title                  <- map["title"]
        url                   <- map["url"]
        published             <- map["published"]
        id                    <- map["id"]
        enabled_for_tvorama   <- map["enabled_for_tvorama"]
        youtube_id            <- map["youtube_id"]
        thumbnail             <- map["thumbnail"]
    }
}
