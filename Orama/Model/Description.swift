//
//  Description.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import ObjectMapper

class Description: Mappable {
    
    var objective: String?
    var movie_url: String?
    var target_audience: String?
    var strengths: String?
    var strategy: String?

    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        objective          <- map["objective"]
        movie_url          <- map["movie_url"]
        target_audience    <- map["target_audience"]
        strengths          <- map["strengths"]
        strategy           <- map["strategy"]
    }
}
