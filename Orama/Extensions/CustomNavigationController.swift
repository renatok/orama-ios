//
//  CustomNavigationController.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import UIKit

extension UINavigationController {
    func setUI() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor.customGreen
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barStyle = .black
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}
