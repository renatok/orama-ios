//
//  CustomTableView.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import UIKit

extension UITableView {
    func setUI(view: UIView) {
        var safeArea: UILayoutGuide!
        safeArea = view.layoutMarginsGuide

        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        self.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        self.register(CustomFundTableViewCell.self, forCellReuseIdentifier: "cell")
        self.rowHeight = 100
        self.tableFooterView = UIView()
    }
}
