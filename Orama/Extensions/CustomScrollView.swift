//
//  CustomScrollView.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import UIKit

extension UIScrollView {
    // Set content size. It will block the mainThread
    func recalculateVerticalContentSize_synchronous () {
        let unionCalculatedTotalRect = recursiveUnionInDepthFor(view: self)
        self.contentSize = CGRect(x: 0, y: 0, width: self.frame.width, height: unionCalculatedTotalRect.height).size;
    }
    
    private func recursiveUnionInDepthFor (view: UIView) -> CGRect {
        var totalRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        // Calculate recursevly for every subView
        for subView in view.subviews {
            totalRect =  totalRect.union(recursiveUnionInDepthFor(view: subView))
        }
        // Return the totalCalculated for all in depth subViews.
        return totalRect.union(view.frame)
    }
}
