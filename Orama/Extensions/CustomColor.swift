//
//  Color.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import UIKit

extension UIColor {
    // Custom Orama main color
    static var customGreen: UIColor {
        return UIColor.init(red: 73.0/255.0, green: 131.0/255.0, blue: 58.0/255.0, alpha: 1)
    }
}
