//
//  FundDetailUI.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import UIKit

extension FundDetailController {

    func setupUI() {
        
        self.title = Constants.DETAIL_TITLE
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        
        // Scroll View
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.isScrollEnabled = true
        
        // Full Name Label
        scrollView.addSubview(lbFullName)
        lbFullName.translatesAutoresizingMaskIntoConstraints = false
        lbFullName.font = UIFont.boldSystemFont(ofSize: 20)
        lbFullName.adjustsFontSizeToFitWidth = true;
        lbFullName.numberOfLines = 0
        lbFullName.textColor = UIColor.customGreen
        lbFullName.textAlignment = .center
        
        // Initial Date Label
        scrollView.addSubview(lbInitialDate)
        lbInitialDate.translatesAutoresizingMaskIntoConstraints = false
        lbInitialDate.font = UIFont.systemFont(ofSize: 20)
        lbInitialDate.adjustsFontSizeToFitWidth = true;
        lbInitialDate.numberOfLines = 0
        lbInitialDate.textAlignment = .center
        
        // Image Thumb
        scrollView.addSubview(ivThumb)
        ivThumb.translatesAutoresizingMaskIntoConstraints = false
        ivThumb.contentMode = .scaleAspectFit
        
        // Fund Manager Description Label
        scrollView.addSubview(lbDescription)
        lbDescription.translatesAutoresizingMaskIntoConstraints = false
        lbDescription.font = UIFont.systemFont(ofSize: 20)
        lbDescription.adjustsFontSizeToFitWidth = true;
        lbDescription.numberOfLines = 0
        lbDescription.textAlignment = .justified
        
        // Main Scroll View
        scrollView.addSubview(btBuy)
        btBuy.translatesAutoresizingMaskIntoConstraints = false
        btBuy.setTitle(Constants.BUY, for: .normal)
        btBuy.addTarget(self, action: #selector(actionBuy), for: .touchUpInside)
        btBuy.backgroundColor = UIColor.customGreen
        
        NSLayoutConstraint.activate([
            lbFullName.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            lbFullName.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            lbFullName.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20),
            lbFullName.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0),
            lbFullName.heightAnchor.constraint(equalToConstant: 70),
            
            lbInitialDate.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            lbInitialDate.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            lbInitialDate.topAnchor.constraint(equalTo: lbFullName.bottomAnchor, constant: 20),
            lbInitialDate.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0),
            lbInitialDate.heightAnchor.constraint(equalToConstant: 30),
            
            ivThumb.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            ivThumb.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            ivThumb.topAnchor.constraint(equalTo: lbInitialDate.bottomAnchor, constant: 20),
            ivThumb.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0),
            ivThumb.heightAnchor.constraint(equalToConstant: 200),
            
            lbDescription.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            lbDescription.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            lbDescription.topAnchor.constraint(equalTo: ivThumb.bottomAnchor, constant: 20),
            
            btBuy.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            btBuy.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            btBuy.topAnchor.constraint(equalTo: lbDescription.bottomAnchor, constant: 20),
            btBuy.heightAnchor.constraint(equalToConstant: 70),
        ])
    }
}
