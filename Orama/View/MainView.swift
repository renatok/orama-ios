//
//  MainView.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import UIKit

extension MainController {
    func setupUI() {
        self.title = Constants.APP_TITLE
        
        view.backgroundColor = .white
        
        // Table View
        view.addSubview(tableView)
        tableView.setUI(view: self.view)
        tableView.isHidden = true

        // Activity Indicator
        self.view.addSubview(activityView)
        activityView.center = self.view.center
        
        // Add Right bar button to navigation bar
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.HISTORIC, style: .plain, target: self, action: #selector(openHistoric))
    }
}
