//
//  HistoricView.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import UIKit

extension HistoricController {
    func setupUI() {
        self.title = Constants.VIEW_TITLE
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        self.navigationController?.setUI()

        // Table View
        view.addSubview(tableView)
        tableView.setUI(view: self.view)
        
        // Add left bar button to navigation bar
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Constants.CLOSE, style: .plain, target: self, action: #selector(closeView))
    }
}
