//
//  CustomFundTableviewCell.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import UIKit

class CustomFundTableViewCell: UITableViewCell {

    let lbName = UILabel()
    let lbOperability = UILabel()
    let lbGenericInfo = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
          super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        lbName.translatesAutoresizingMaskIntoConstraints = false
        lbName.font = UIFont.boldSystemFont(ofSize: 20)
        lbName.textColor = UIColor.customGreen
        
        lbOperability.translatesAutoresizingMaskIntoConstraints = false
        lbOperability.font = UIFont.systemFont(ofSize: 14)
        
        lbGenericInfo.translatesAutoresizingMaskIntoConstraints = false
        lbGenericInfo.font = UIFont.systemFont(ofSize: 14)
        
        // Add the UI components
        contentView.addSubview(lbName)
        contentView.addSubview(lbOperability)
        contentView.addSubview(lbGenericInfo)

        NSLayoutConstraint.activate([
            lbName.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            lbName.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -50),
            lbName.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            lbName.heightAnchor.constraint(equalToConstant: 50),
            
            lbOperability.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            lbOperability.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30),
            lbOperability.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            lbOperability.heightAnchor.constraint(equalToConstant: 30),
            
            lbGenericInfo.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            lbGenericInfo.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            lbGenericInfo.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            lbGenericInfo.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

