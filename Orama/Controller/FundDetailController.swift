//
//  FundDetailViewController.swift
//  Orama
//
//  Created by Renato Kuroe on 28/09/20.
//

import Foundation
import UIKit
import ImageLoader

class FundDetailController: UIViewController {
    
    var fund: Fund!
    var safeArea: UILayoutGuide!
    let scrollView = UIScrollView()
    let lbFullName = UILabel()
    let lbInitialDate = UILabel()
    let ivThumb = UIImageView()
    let lbDescription = UILabel()
    let btBuy = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setValues()
    }
    
    // MARK: - VALUES
    
    func setValues()  {
        lbFullName.text = fund.full_name
        lbInitialDate.text = Constants.INITIAL_DATE + Utils.formatDate(dateString: fund.initial_date!)
        lbDescription.text = fund.fund_manager?.description
        
        // Load image if exists. Otherwise, set placeholder instead
        let urlThumb = NSURL(string: fund.strategy_video?.thumbnail ?? "")
        ivThumb.load.request(with: urlThumb! as URL,
                             placeholder: UIImage(named: "orama_logo"),
                             onCompletion: { _,_,_  in
                                DispatchQueue.main.async {
                                    // Set Scrollview content size after load image
                                    self.scrollView.isScrollEnabled = true
                                    self.scrollView.recalculateVerticalContentSize_synchronous()
                                }
                             })
    }
    
    // MARK: - ACTION BUY AND VALIDATES
    
    @objc func actionBuy(sender: UIButton!) {
        alertWithValueInput()
    }
    
    // Build text field alert for amount input
    func alertWithValueInput() {
        let ac = UIAlertController(title: Constants.HOW_MUCH, message: nil, preferredStyle: .alert)
        ac.addTextField()
        ac.textFields![0].keyboardType = .numberPad
        ac.textFields![0].addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        
        let submitAction = UIAlertAction(title: Constants.BUY, style: .default) { [unowned ac] _ in
            let valueString = ac.textFields![0]
            if (self.checkMinimumApplication(valueSelected: valueString.text ?? "0")){
                HistoricStorage.addToFundBoughtList(name: self.fund.simple_name!,
                                                    added: NSDate() as Date,
                                                    amount: valueString.text!)
            }
            
            Utils.showAlert(vc: self, message: Constants.BUY_SUCCESS)
        }
        
        let cancelAction = UIAlertAction(title: Constants.CANCEL, style: .cancel) { [unowned ac] _ in
            ac.dismiss(animated: true, completion: nil)
        }
        
        ac.addAction(submitAction)
        ac.addAction(cancelAction)
        present(ac, animated: true)
    }
    
    // Verify if amount to invest is greather than minimum application value
    func checkMinimumApplication(valueSelected: String) -> Bool {
        let minimumApplication = Float.init((fund.operability!.minimum_initial_application_amount)!)!
        let newValue = Utils.currencyToFloat(currency: valueSelected)
        if newValue < minimumApplication {
            Utils.showAlert(vc: self, message: Constants.MINIMUM_VALUE +
                                Utils.formatCurrency(value: (fund.operability?.minimum_initial_application_amount)!))
            return false
        }
        return true
    }
    
    // MARK: - TEXT FIELD DELEGATE
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }
}

