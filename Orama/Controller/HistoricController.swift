//
//  HistoricViewController.swift
//  Orama
//
//  Created by Renato Kuroe on 29/09/20.
//

import UIKit
import Alamofire
import ObjectMapper

class HistoricController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    var funds: [FundBought]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDelegates()
        loadData()
    }
    
    @objc func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - LOAD PERSISTENT DATA

    func loadData() {
        funds = Storage.fundBoughtList.get()
        if (funds == nil) {
            Utils.showAlert(vc: self, message: Constants.NO_HISTORIC)
        } else {
            tableView.reloadData()
        }
    }
    // MARK: - TABLE VIEW DATA SOURCE AND DELEGATES
    
    func setupDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.funds?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomFundTableViewCell
        let fund = self.funds![indexPath.row]
        
        cell.lbName.text = fund.name
        cell.lbOperability.text = Constants.APPLICATION_MADE + fund.amount
        cell.lbGenericInfo.text = Constants.APPLICATION_DATE + Utils.dateToString(date: fund.added)
        cell.selectionStyle = .none
        
        return cell
    }
}
