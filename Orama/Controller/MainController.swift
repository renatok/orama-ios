//
//  ViewController.swift
//  Orama
//
//  Created by Renato Kuroe on 25/09/20.
//

import UIKit
import Alamofire
import ObjectMapper

class MainController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let tableView = UITableView()
    var activityView = UIActivityIndicatorView(style: .gray)
    var funds: [Fund]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setupUI()
        setupDelegates()
    }
    
    @objc func openHistoric() {
        self.present(UINavigationController(rootViewController: HistoricController()), animated: true, completion: nil)
    }
    
    // MARK: - API REQUEST
    
    func loadData() {
        activityView.startAnimating()
        
        let jsonDataUrl = Constants.URL_FUND_DETAIL_FULL
        Alamofire.request(jsonDataUrl).responseJSON { response in
            if response.result.isSuccess {
                // Set all data received and populate table view
                self.funds = Mapper<Fund>().mapArray(JSONObject: response.result.value)!
                self.tableView.reloadData()
                self.tableView.isHidden = false
                self.activityView.stopAnimating()
                
            } else if (response.result.isFailure) {
                //Manager error
                switch (response.error!._code){
                case NSURLErrorTimedOut:
                    Utils.showAlert(vc: self, message: Constants.ALERT_ERROR_TIMEOUT)
                    break
                case NSURLErrorNotConnectedToInternet:
                    Utils.showAlert(vc: self, message: Constants.ALERT_ERROR_CONNECTION_FAILED)
                    break
                default:
                    Utils.showAlert(vc: self, message: Constants.ALERT_ERROR_GENERIC)
                }
                self.activityView.stopAnimating()
            }
            
        }
    }
    
    // MARK: - TABLE VIEW DATA SOURCE AND DELEGATES
    
    func setupDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.funds?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomFundTableViewCell
        let fund = self.funds![indexPath.row]
        
        cell.lbName.text = fund.simple_name
        cell.lbOperability.text = Constants.MINIMUM_APPLICATION + Utils.formatCurrency(value: fund.operability!.minimum_initial_application_amount!)
        cell.lbGenericInfo.text = (fund.specification?.fund_risk_profile!.name!)!
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fundSelected = self.funds![indexPath.row]
        let fundDetailVc = FundDetailController()
        fundDetailVc.fund = fundSelected
        self.navigationController?.pushViewController(fundDetailVc, animated: true)
        
        // Desmarcar seleção quando clicado em um item para não persistir
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
